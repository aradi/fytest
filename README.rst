===================================================
FyTest — Instant unit testing framework for Fortran
===================================================

FyTest is a lightweight unit testing framework for Fortran. It can be easily
bundled with every modern Fortran project, providing a versatile unit testing
solution out of the box. By using the powerful `Fypp preprocessor
<http://bitbucket.org/aradi/fypp>`_, it enables the programmer to concentrate on
the essential part of each tests, and sets up all the code necessary to
integrate them into a unit testing framework automatically.

At the current early stage of development, no extensive documentation is
available yet. Have a look at the examples in the ``examples/`` folder for
demonstration of usage.

The project is `hosted on bitbucket <http://bitbucket.org/aradi/fytest>`_.

FyTest is released under the *BSD 2-clause license*.
