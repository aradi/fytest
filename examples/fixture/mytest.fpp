#:include 'fytest.fypp'

#:call TEST_MODULE('MyTest')
  implicit none

  type, extends(TestCase) :: InitializedTest
    integer, allocatable :: data(:)
  contains
    procedure :: setUp
    procedure :: tearDown
  end type InitializedTest
    
  
contains

  subroutine setUp(this)
    class(InitializedTest), intent(inout) :: this

    this%data = [1, 2, 3, 4, 5]
    
  end subroutine setUp


  subroutine tearDown(this)
    class(InitializedTest), intent(inout) :: this

    ! Only for demonstration purposes as data would be deallocated anyway
    if (allocated(this%data)) then
      deallocate(this%data)
    end if

  end subroutine tearDown
  
  
  #:call TEST_F('Trivial1', 'InitializedTest')
    @:EXPECT(allocated(this%data))
    @:EXPECT(size(this%data) > 0)
    @:EXPECT(this%data(1) == 1)
  #:endcall TEST_F

    
  #:call TEST_F('Trivial2', 'InitializedTest')
    integer :: ii, jj

    ii = 0
    jj = ii
    do while (jj < 100000000)
      ii = ii + 1
      jj = ii
    end do
    @:EXPECT(size(this%data) >= 6)
    @:EXPECT(this%data(4) == 1, "Invalid comparison")
    @:EXPECT(this%data(1) == 1)
  #:endcall TEST_F

#:endcall TEST_MODULE


$:SERIAL_DRIVER()
