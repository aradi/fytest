#:include 'fytest.fypp'

#:call TEST_MODULE('MyTest')
  use mpi
  implicit none
  
contains
  
  #:call MPI_TEST('Trivial1')
    integer :: mysize, myrank, error

    call mpi_comm_size(this%comm, mysize, error)
    call mpi_comm_rank(this%comm, myrank, error)
    @:EXPECT(myrank < mysize)
    ! Should fail if running more than one processes
    @:EXPECT(myrank == 0, "My rank should be one")

  #:endcall MPI_TEST

#:endcall TEST_MODULE


$:MPI_DRIVER()
