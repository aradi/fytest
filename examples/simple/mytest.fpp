#:include 'fytest.fypp'

#:call TEST_MODULE('MyTest')
  implicit none
  
contains
  
  #:call TEST('Trivial1')
    @:EXPECT('1' == '1')
  #:endcall TEST

    
  #:call TEST('Trivial2')
    integer :: ii, jj
    ii = 0
    jj = ii
    do while (jj < 100000000)
      ii = ii + 1
      jj = ii
    end do
    @:EXPECT('1' == '2', "Invalid comparison")
    @:EXPECT('1' == '1')
  #:endcall TEST

#:endcall TEST_MODULE


$:SERIAL_DRIVER()
