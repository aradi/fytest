#:mute
#:set _nAllTests = 0
#:set _testModules = []
#:set _unassignedTests = []

  
#:def _quote(txt)
'${txt.replace("'", "''")}$'
#!"
#:enddef


#:def ASSERT(cond, msg=None)
    $:EXPECT(cond, msg)
    if (.not. (${cond}$)) then
      return
    end if
#:enddef ASSERT


#:def EXPECT(cond, msg=None)
  #:set msg = _quote(cond) if msg is None else msg
    call this%check_(${cond}$, ${_quote(_FILE_)}$, ${_LINE_}$, ${msg}$)
#:enddef EXPECT


#:def TEST(name, code, fixture=None)
  #:mute
    $:setvar('fname', 'test_' + name.lower())
    $:_unassignedTests.append({'name': name, 'fname': fname, 'fixture': fixture})
    $:setvar('testCase', (fixture or 'TestCase'))
  #:endmute
  subroutine ${fname}$(this)
    class(${testCase}$), intent(inout) :: this
    $:code
  end subroutine ${fname}$
#:enddef TEST


#:def TEST_F(name, fixture, code)
  $:TEST(name, code, fixture=fixture)
#:enddef TEST_F


#:def MPI_TEST(name, code)
  $:TEST(name, code, fixture='MpiTestCase')
#:enddef


#:def TEST_MODULE(name, code)
  #:mute
    $:globalvar('_nAllTests', '_unassignedTests')
    $:setvar('fname', 'testmod_' + name.lower())
    $:_testModules.append({'name': name, 'fname': fname, 'tests': _unassignedTests})
    $:setvar('_nAllTests', _nAllTests + len(_unassignedTests))
    $:setvar('_unassignedTests', [])
  #:endmute
module ${fname}$
  use fytest
  $:code
end module ${fname}$
#:enddef TEST_MODULE  


#:def SERIAL_DRIVER()
  $:_DRIVER(mpi=False)
#:enddef

#:def MPI_DRIVER()
  $:_DRIVER(mpi=True)
#:enddef


#:def _DRIVER(mpi)
program fytest_driver
#:if mpi
  use mpi
#:endif
  use fytest
  implicit none

  integer, parameter :: NR_TESTS = ${_nAllTests}$
  type(TestResult) :: results(NR_TESTS)
  real :: timeModStart, timeModEnd, timeDriverStart, timeDriverEnd
#:if mpi
  type(MpiLogger) :: myLogger
  integer :: mpierror
#:else
  type(Logger) :: myLogger
#:endif

#:if mpi
  call mpi_init(mpierror)
  if (mpierror /= 0) then
    stop "MPI could not be initialized"
  end if
  call MpiLogger_init_(myLogger, MPI_COMM_WORLD)
#:endif
  
  call myLogger%reportTestDriverStart(${_nAllTests}$, ${len(_testModules)}$)
  call cpu_time(timeDriverStart)

#:set ind = 1
#:for testmod in _testModules
  call myLogger%reportTestModuleStart("${testmod['name']}$", ${len(testmod['tests'])}$)
  call cpu_time(timeModStart)

#:for test in testmod['tests']
  call myLogger%reportTestStart("${testmod['name']}$", "${test['name']}$")
  call ${testmod['fname']}$_${test['fname']}$(myLogger, results(${ind}$))
  call myLogger%reportTestEnd(results(${ind}$))
  
#:set ind = ind + 1
#:endfor
  call cpu_time(timeModEnd)
  call myLogger%reportTestModuleEnd("${testmod['name']}$", ${len(testmod['tests'])}$,&
      & timeModEnd - timeModStart)
  
#:endfor
  call cpu_time(timeDriverEnd)
  call myLogger%reportTestDriverEnd(${_nAllTests}$, ${len(_testModules)}$,&
      & timeDriverEnd - timeDriverStart)
  call myLogger%reportTestResults(results)

#:if mpi
  call mpi_finalize(mpierror)
#:endif

contains

#:for testmod in _testModules
#:for test in testmod['tests']

  subroutine ${testmod['fname']}$_${test['fname']}$(myLogger, myResult)
    ! Workaround: GFortran crashes without explicit import of fytest
    use fytest
  #:if test['fixture'] is None
    use ${testmod['fname']}$, only: ${test['fname']}$
  #:else
    use ${testmod['fname']}$, only: ${test['fname']}$, ${test['fixture']}$
  #:endif
    class(Logger), intent(in) :: myLogger
    type(TestResult), intent(out) :: myResult

  #:set testCase = (test['fixture'] or 'TestCase')
    type(${testCase}$) :: test

  #:if test['fixture'] is None
    call TestCase_init_(test, "${testmod['name']}$", "${test['name']}$", myLogger)
      #:else
    call TestCase_init_(test%TestCase, "${testmod['name']}$", "${test['name']}$", myLogger)
  #:endif
    call test%startTest_()
    call test%setUp()
    if (test%getStatus_() == TEST_STATUS%SUCCEEDED) then
      call ${test['fname']}$(test)
    end if
    call test%tearDown()
    call test%finishTest_()
    call test%getResult_(myResult)

  end subroutine ${testmod['fname']}$_${test['fname']}$

#:endfor
#:endfor

end program fytest_driver
  
#:enddef _DRIVER
    
#:endmute
