module fytest_iohelpers
  implicit none
  private

  public :: selectNumber
  public :: getTimingStr


contains

  function selectNumber(singular, plural, occurance) result(noun)
    character(*), intent(in) :: singular
    character(*), intent(in) :: plural
    integer, intent(in) :: occurance
    character(:), allocatable :: noun

    if (abs(occurance) == 1) then
      noun = singular
    else
      noun = plural
    end if

  end function selectNumber


  function getTimingStr(execTime) result(timing)
    real, intent(in) :: execTime
    character(:), allocatable :: timing

    character(100) :: buffer

    if (execTime < 1.0) then
      write(buffer, '(I0,A)') int(execTime * 1000.0), ' ms'
    else if (execTime < 10.0) then
      write(buffer, '(F0.3,A)') execTime, ' s'
    else if (execTime < 100.0) then
      write(buffer, '(F0.2,A)') execTime, ' s'
    else if (execTime < 1000.0) then
      write(buffer, '(F0.1,A)') execTime, ' s'
    else
      write(buffer, '(I0,A)') int(execTime), ' s'
    end if
    timing = trim(buffer)

  end function getTimingStr


end module fytest_iohelpers
