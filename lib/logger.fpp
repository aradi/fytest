module fytest_logger
  use, intrinsic :: iso_fortran_env, only : stdout => output_unit
  use fytest_testresult
  use fytest_iohelpers
  implicit none
  private

  public :: Logger

  type :: Logger
  contains
    procedure :: reportTestStart
    procedure :: reportTestEnd
    procedure :: reportTestModuleStart
    procedure :: reportTestModuleEnd
    procedure :: reportTestDriverStart
    procedure :: reportTestDriverEnd
    procedure :: reportTestResults
    procedure :: writeMessage
  end type Logger


contains

  subroutine writeMessage(this, msg)
    class(Logger), intent(inout) :: this
    character(*), intent(in) :: msg

    write(stdout, '(A)') msg

  end subroutine writeMessage
  

  subroutine reportTestStart(this, moduleName, testName)
    class(Logger), intent(inout) :: this
    character(*), intent(in) :: moduleName
    character(*), intent(in) :: testName

    write(stdout, '(4A)') '[ RUN      ] ', moduleName, '.', testName

  end subroutine reportTestStart


  subroutine reportTestEnd(this, testRes)
    class(Logger), intent(inout) :: this
    type(TestResult), intent(in) :: testRes

    select case (testRes%status)
    case (TEST_STATUS%SUCCEEDED)
      write(stdout, '(7A)') '[       OK ] ', testRes%moduleName, '.', testRes%testName, ' (',&
          & getTimingStr(testRes%execTime), ')'
    case (TEST_STATUS%FAILED)
      write(stdout, '(7A)') '[  FAILED  ] ', testRes%moduleName, '.', testRes%testName, ' (',&
          & getTimingStr(testRes%execTime), ')'
    case (TEST_STATUS%NOT_RUN)
      write(stdout, '(4A)') '[ NOT RUN  ] ', testRes%moduleName, '.', testRes%testName
    end select

  end subroutine reportTestEnd


  subroutine reportTestModuleStart(this, moduleName, nTests)
    class(Logger), intent(inout) :: this
    character(*), intent(in) :: moduleName
    integer, intent(in) :: nTests

    write(stdout, '(A,I0,4A)') '[----------] Running ', nTests, ' ',&
        & selectNumber('test', 'tests', nTests), ' from module ', moduleName
    
  end subroutine reportTestModuleStart


  subroutine reportTestModuleEnd(this, moduleName, nTests, execTime)
    class(Logger), intent(inout) :: this
    character(*), intent(in) :: moduleName
    integer, intent(in) :: nTests
    real, intent(in) :: execTime

    write(stdout, '(A,I0,7A)') '[----------] Finished ', nTests, ' ',&
        & selectNumber('test', 'tests', nTests), ' from module ', moduleName, ' (',&
        & getTimingStr(execTime), ')'
    
  end subroutine reportTestModuleEnd


  subroutine reportTestDriverStart(this, nAllTests, nTestModules)
    class(Logger), intent(inout) :: this
    integer, intent(in) :: nAllTests
    integer, intent(in) :: nTestModules
    
    write(stdout, '(A,I0,3A,I0,3A)') '[==========] Running ', nAllTests, ' ',&
        & selectNumber('test', 'tests', nAllTests), ' from ', nTestModules,&
        & ' test ', selectNumber('module', 'modules', nTestModules)

  end subroutine reportTestDriverStart


  subroutine reportTestDriverEnd(this, nAllTests, nTestModules, execTime)
    class(Logger), intent(inout) :: this
    integer, intent(in) :: nAllTests
    integer, intent(in) :: nTestModules
    real, intent(in) :: execTime
    
    write(stdout, '(A,I0,3A,I0,5A)') '[==========] Finished ', nAllTests, ' ',&
        & selectNumber('test', 'tests', nAllTests), ' from ', nTestModules,&
        & ' test ', selectNumber('module', 'modules', nTestModules), ' (',&
        & getTimingStr(execTime), ')'

  end subroutine reportTestDriverEnd

  
  subroutine reportTestResults(this, testResults)
    class(Logger), intent(inout) :: this
    type(TestResult), intent(in) :: testResults(:)

    integer :: succeeded, failed, remaining
    integer :: ind

    succeeded = count(testResults%status == TEST_STATUS%SUCCEEDED)
    write(stdout, '(A,I0,A)') '[  PASSED  ] ', succeeded,&
        & selectNumber(' test.', ' tests.', succeeded)
    failed = count(testResults%status == TEST_STATUS%FAILED)
    if (failed > 0) then
      write(stdout, '(A,I0,2A)') '[  FAILED  ] ', failed,&
          & selectNumber(' test', ' tests', succeeded), ', listed below:'
      remaining = failed
      do ind = 1, size(testResults)
        associate (testRes => testResults(ind))
          if (testRes%status == TEST_STATUS%FAILED) then
            write(stdout, '(4A)') '[  FAILED  ] ', testRes%moduleName, '.', testRes%testName
            remaining = remaining - 1
          end if
        end associate
        if (remaining == 0) then
          exit
        end if
      end do
    end if
    write(stdout, *)
    if (failed == 0) then
      write(stdout, '(A)') 'ALL TESTS PASSED'
    else
      write(stdout, '(I0,3A)') failed, selectNumber(' TEST', ' TESTS', failed), ' FAILED'
    end if
    
  end subroutine reportTestResults

  
end module fytest_logger
