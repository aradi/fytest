module fytest
  use fytest_testcase
  use fytest_testresult
  use fytest_logger
#:if defined('MPI')  
  use fytest_mpitestcase
  use fytest_mpilogger
#:endif  
  implicit none
  
end module fytest
