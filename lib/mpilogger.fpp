module fytest_mpilogger
  use mpi
  use fytest_logger
  use fytest_testresult
  implicit none
  private

  public :: MpiLogger, MpiLogger_init_

  type, extends(Logger) :: MpiLogger
    private
    integer :: rank
    logical :: master
  contains
    procedure :: reportTestStart
    procedure :: reportTestEnd
    procedure :: reportTestModuleStart
    procedure :: reportTestModuleEnd
    procedure :: reportTestDriverStart
    procedure :: reportTestDriverEnd
    procedure :: reportTestResults
    procedure :: writeMessage
  end type MpiLogger


contains

  subroutine MpiLogger_init_(this, comm)
    type(MpiLogger), intent(out) :: this
    integer, intent(in) :: comm

    integer :: mpierror
    
    call mpi_comm_rank(comm, this%rank, mpierror)
    this%master = (this%rank == 0)

  end subroutine MpiLogger_init_


  subroutine writeMessage(this, msg)
    class(MpiLogger), intent(inout) :: this
    character(*), intent(in) :: msg

    character(10) :: buffer

    write(buffer, '(I0)') this%rank
    call this%Logger%writeMessage('p' // trim(buffer) // '> ' // msg)

  end subroutine writeMessage
  

  subroutine reportTestStart(this, moduleName, testName)
    class(MpiLogger), intent(inout) :: this
    character(*), intent(in) :: moduleName
    character(*), intent(in) :: testName

    if (this%master) then
      call this%Logger%reportTestStart(moduleName, testName)
    end if

  end subroutine reportTestStart


  subroutine reportTestEnd(this, testRes)
    class(MpiLogger), intent(inout) :: this
    type(TestResult), intent(in) :: testRes

    if (this%master) then
      call this%Logger%reportTestEnd(testRes)
    end if

  end subroutine reportTestEnd


  subroutine reportTestModuleStart(this, moduleName, nTests)
    class(MpiLogger), intent(inout) :: this
    character(*), intent(in) :: moduleName
    integer, intent(in) :: nTests

    if (this%master) then
      call this%Logger%reportTestModuleStart(moduleName, nTests)
    end if
    
  end subroutine reportTestModuleStart


  subroutine reportTestModuleEnd(this, moduleName, nTests, execTime)
    class(MpiLogger), intent(inout) :: this
    character(*), intent(in) :: moduleName
    integer, intent(in) :: nTests
    real, intent(in) :: execTime

    if (this%master) then
      call this%Logger%reportTestModuleEnd(moduleName, nTests, execTime)
    end if
    
  end subroutine reportTestModuleEnd


  subroutine reportTestDriverStart(this, nAllTests, nTestModules)
    class(MpiLogger), intent(inout) :: this
    integer, intent(in) :: nAllTests
    integer, intent(in) :: nTestModules

    if (this%master) then
      call this%Logger%reportTestDriverStart(nAllTests, nTestModules)
    end if

  end subroutine reportTestDriverStart


  subroutine reportTestDriverEnd(this, nAllTests, nTestModules, execTime)
    class(MpiLogger), intent(inout) :: this
    integer, intent(in) :: nAllTests
    integer, intent(in) :: nTestModules
    real, intent(in) :: execTime

    if (this%master) then
      call this%Logger%reportTestDriverEnd(nAllTests, nTestModules, execTime)
    end if

  end subroutine reportTestDriverEnd

  
  subroutine reportTestResults(this, testResults)
    class(MpiLogger), intent(inout) :: this
    type(TestResult), intent(in) :: testResults(:)

    integer :: succeeded, failed, remaining
    integer :: ind

    if (this%master) then
      call this%Logger%reportTestResults(testResults)
    end if
    
  end subroutine reportTestResults

  
end module fytest_mpilogger
