module fytest_mpitestcase
  use mpi
  use fytest_testcase
  use fytest_testresult
  use fytest_logger
  implicit none
  private

  public :: MpiTestCase, MpiTestCase_init_


  type, extends(TestCase) :: MpiTestCase
    integer :: comm
  contains
    !procedure :: startTest_
    !procedure :: finishTest_
    procedure :: check_
  end type MpiTestCase


contains

  !> Sets up a test environment.
  !!
  subroutine setUp(this)

    !> Instance.
    class(TestCase), intent(inout) :: this

    continue
    
  end subroutine setUp


  !> Tears down a test environment.
  !!
  subroutine tearDown(this)

    !> Instance
    class(TestCase), intent(inout) :: this

    continue

  end subroutine tearDown

  
  subroutine MpiTestCase_init_(this, moduleName, testName, testLogger, comm)
    type(MpiTestCase), intent(out) :: this
    character(*), intent(in) :: moduleName
    character(*), intent(in) :: testName
    type(Logger), intent(in) :: testLogger
    integer, intent(in) :: comm

    call TestCase_init_(this%TestCase, moduleName, testName, testLogger)
    this%comm = comm
    
  end subroutine MpiTestCase_init_


  subroutine check_(this, cond, file, line, msg)
    class(MpiTestCase), intent(inout) :: this
    logical, intent(in) :: cond
    character(*), intent(in) :: file
    integer, intent(in) :: line
    character(*), intent(in) :: msg

    logical :: reducedCond
    integer :: error

    reducedCond = cond
    call mpi_allreduce(MPI_IN_PLACE, reducedCond, 1, MPI_LOGICAL, MPI_LAND, this%comm, error)
    if (.not. reducedCond) then
      this%myResult%status = TEST_STATUS%failed
      this%myResult%file = file
      this%myResult%line = line
    end if
    if (.not. cond) then
      call this%myLogger%writeMessage(msg)
    end if
    
  end subroutine check_
    

end module fytest_mpitestcase
