module fytest_serialtestcase
  use fytest_testcase
  implicit none
  private

  public :: SerialTestCase

  type, extends(TestCase) :: SerialTestCase
  end type SerialTestCase

end module fytest_serialtestcase
