module fytest_testcase
  use fytest_testresult
  use fytest_logger
  implicit none
  private

  public :: TestCase, TestCase_init_

  type :: TestCase
    type(TestResult) :: myResult
    class(Logger), allocatable :: myLogger
    real :: startTime = -1.0
  contains
    procedure :: setUp
    procedure :: tearDown
    procedure :: startTest_
    procedure :: finishTest_
    procedure :: check_
    procedure :: getResult_
    procedure :: getStatus_
  end type TestCase


contains

  !> Sets up a test environment.
  !!
  subroutine setUp(this)

    !> Instance.
    class(TestCase), intent(inout) :: this

    continue
    
  end subroutine setUp


  !> Tears down a test environment.
  !!
  subroutine tearDown(this)

    !> Instance
    class(TestCase), intent(inout) :: this

    continue

  end subroutine tearDown


  
  subroutine TestCase_init_(this, moduleName, testName, testLogger)
    type(TestCase), intent(out) :: this
    character(*), intent(in) :: moduleName
    character(*), intent(in) :: testName
    class(Logger), intent(in) :: testLogger

    this%myResult%moduleName = moduleName
    this%myResult%testName = testName
    this%myResult%status = TEST_STATUS%NOT_RUN
    allocate(this%myLogger, source=testLogger)
    
  end subroutine TestCase_init_


  subroutine startTest_(this)
    class(TestCase), intent(inout) :: this

    call cpu_time(this%startTime)
    this%myResult%status = TEST_STATUS%SUCCEEDED

  end subroutine startTest_
  

  subroutine finishTest_(this)
    class(TestCase), intent(inout) :: this
    
    real :: endTime

    if (this%startTime /= -1.0) then
      call cpu_time(endTime)
      this%myResult%execTime = endTime - this%startTime
    else
      this%myResult%execTime = -1.0
    end if
    
  end subroutine finishTest_


  function getStatus_(this) result(status)
    class(TestCase), intent(in) :: this
    integer :: status

    status = this%myResult%status

  end function getStatus_


  subroutine getResult_(this, myResult)
    class(TestCase), intent(in) :: this
    type(TestResult), intent(out) :: myResult

    myResult = this%myResult

  end subroutine getResult_


  subroutine check_(this, cond, file, line, msg)
    class(TestCase), intent(inout) :: this
    logical, intent(in) :: cond
    character(*), intent(in) :: file
    integer, intent(in) :: line
    character(*), intent(in) :: msg

    if (.not. cond) then
      this%myResult%status = TEST_STATUS%failed
      this%myResult%file = file
      this%myResult%line = line
      call this%myLogger%writeMessage(msg)
    end if

  end subroutine check_
    

end module fytest_testcase
