module fytest_testresult
  implicit none
  private

  public :: TEST_STATUS
  public :: TestResult


  type :: TestStatusEnum
    integer :: NOT_RUN
    integer :: SUCCEEDED
    integer :: FAILED
  end type TestStatusEnum


  type(TestStatusEnum), parameter :: TEST_STATUS =&
      & TestStatusEnum(0, 1, 2)


  type :: TestResult
    integer :: status = TEST_STATUS%NOT_RUN
    character(:), allocatable :: testName, moduleName
    character(:), allocatable :: file
    integer :: line
    real :: execTime
  end type TestResult


end module fytest_testresult
